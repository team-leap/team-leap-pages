function updateContent(index) {
  var descriptions = [
    "Der Raspberry Pi der 3. Generation ist das Herzstück unseres Rocket Deployer, über ihn laufen alle RFID Reader und Inputs. Mit seinem kleinen Formfaktor passt er in jedes Gehäuse.",
    "Die soliden MFRC522 Reader lesen und schreiben zuverlässig die RFID Karten. Die Arbeit mit den Readern ist sehr einfach dank Python Modulen.",
    "Die RFID Karten repräsentieren verschiedene Module mit welchen man die Applikation zusammenbauen kann."
  ];

  var descriptionElement = document.getElementById('description');
  var dots = document.querySelectorAll('.controller.dots li');

  // Update the description
  descriptionElement.textContent = descriptions[index];

  // Update the selected dot
  dots.forEach(function(dot, i) {
    if (i === index) {
      dot.classList.add('selected');
    } else {
      dot.classList.remove('selected');
    }
  });
}