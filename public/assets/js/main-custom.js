function isHttp() {
    return window.location.protocol === 'http:';
}

function isLocalhost() {
    return window.location.hostname === 'localhost';
}

function redirectToHttps() {
    if (isHttp() && !isLocalhost()) {
        window.location.protocol = 'https:';
    }
}

/**
 * find all elements like: <span class="auto-age" data-birth="2000-05-07">22</span>
 * and calculate the age from the data-birth attribute
 */
function autoAge() {
    let elements = document.getElementsByClassName('auto-age');
    for (let i = 0; i < elements.length; i++) {
        let element = elements[i];
        let birth = element.getAttribute('data-birth');
        let age = calculateAge(birth);
        element.innerHTML = age;
    }
}

function calculateAge(birth) {
    let now = new Date();
    let birthDate = new Date(birth);
    let age = now.getFullYear() - birthDate.getFullYear();
    let m = now.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && now.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

redirectToHttps();
autoAge();


const confettiClickElements = document.querySelectorAll('.confetti-click');
const colors = [
    '#f44336', '#e91e63', '#9c27b0', '#673ab7', '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4',
    '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722'
];

function createConfetti(createOnElement) {
    const confetti = document.createElement('div');

    confetti.style.width = '10px';
    confetti.style.height = '10px';
    confetti.style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
    confetti.style.borderRadius = '50%';
    confetti.style.position = 'absolute';
    confetti.style.zIndex = '10001';

    document.body.appendChild(confetti);

    let boundingRect = createOnElement.getBoundingClientRect();
    let x = boundingRect.left + (Math.random() * boundingRect.width) + window.scrollX;
    let y = boundingRect.top + (Math.random() * boundingRect.height) + window.scrollY;
    confetti.style.left = x + 'px';
    confetti.style.top = y + 'px';

    confetti.motion = [randomIntFromInterval(-200, 200), randomIntFromInterval(-600, -400)];
    let animation = setInterval(function () {
        confetti.motion[1] += 24;
        x += (confetti.motion[0] / 100) * 4;
        y += (confetti.motion[1] / 100) * 4;
        confetti.style.left = x + 'px';
        confetti.style.top = y + 'px';
    }, 25);

    setTimeout(function () {
        clearInterval(animation);
        confetti.remove();
    }, randomIntFromInterval(2000, 4000));
}

for (let i = 0; i < confettiClickElements.length; i++) {
    confettiClickElements[i].addEventListener('click', function () {
        for (let j = 0; j < 10; j++) {
            createConfetti(confettiClickElements[i]);
        }
    });
}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
